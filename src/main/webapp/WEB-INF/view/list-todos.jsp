<%@include file="../common/header.jspf" %>

<%@include file="../common/navbar.jspf" %>

<div class="container">

    <h1> Bem Vindo: ${name} </h1>
    <hr>

    <!-- cria uma tabela que lista as tarefas -->

    <table class="table table-responsive">
        <caption>Lista de Tarefas:</caption>
        <thead>
        <th>Descricao</th>
        <th>Categoria</th>
        <th>Accao</th>
        </thead>
        <tbody>
        <c:forEach items="${todos}" var="todo">
        <tr>
            <td>${todo.name}</td>
            <td>${todo.category}</td>
            <td><a class="btn btn-danger"
                   href="/delete-todo.do?todo=${todo.name}&category=${todo.category}">Eliminar</a></td>
        </tr>
        </tbody>
        </c:forEach>
    </table>

    <br>
    <br>

    <a class="btn btn-success" href="/add-todo.do">Adicionar nova tarefa</a>

</div>

<%@include file="../common/footer.jspf" %>