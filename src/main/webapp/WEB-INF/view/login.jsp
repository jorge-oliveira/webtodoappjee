<%--
  Created by IntelliJ IDEA.
  User: mr_mufy
  Date: 26/03/2018
  Time: 21:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Pagina de login from jsp</title>
</head>

<body>
<html>
<head>
    <link href="webjars/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
    <title>Lista tarefas</title>
</head>

<body>

<!-- NavBar -->
<nav class="navbar navbar-default">

    <a href="/" class="navbar-brand">Brand</a>

    <ul class="nav navbar-nav">
        <li class="active">
            <a href="#">Home</a>
        </li>
        <li>
            <a href="/todo.do">Todos</a>
        </li>
        <li>
            <a href="http://cjoliveira.pt">CjOliveira</a>
        </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a href="/login.do">Login</a></li>
    </ul>

</nav>


<div class="container">

    <br>
    <h1> Faça Login </h1>

    <br>

    <hr>

    <!-- Formulario login -->
    <div class="form-group">
        <form action="/login.do" method="post">
            <p style="color: red"><strong> ${errorMessage} </strong></p>
            <label>
                Nome
            </label>
            <input class="form-control form-group-sm" type="text" name="name"/>
            <label>
                Password
            </label>
            <input class="form-control form-group-sm" type="password" name="password"/>
            <br>
            <input class="btn btn-primary" type="submit" value="Login"/>
        </form>
    </div>
</div>

<%@include file="../common/footer.jspf" %>