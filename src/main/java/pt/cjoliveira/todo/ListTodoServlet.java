package pt.cjoliveira.todo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// informa o URL para ser acedido
@WebServlet(urlPatterns = "/list-todos.do")
public class ListTodoServlet extends HttpServlet {

    private TodoService todoService = new TodoService();

    // tratar de metodos GET
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("todos", todoService.retrieveTodos());

        // redirecionar para a pagina pretendida
        request.getRequestDispatcher("/WEB-INF/view/list-todos.jsp").forward(request, response);
    }

    // tratar de metodos Post
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // obter o parametro preenchido no form
        String newTodo = request.getParameter("todo");
        String category = request.getParameter("category");

        // adiciona o parametro como uma nova tarefa
        todoService.addTodo(new Todo(newTodo, category));

        // redirecionar para a pagina pretendida mas sem enviar novamente a task request.getRequestDispatcher
        // fazemos logo o reenvio para pagina
        response.sendRedirect("/list-todos.do");
    }
}



