package pt.cjoliveira.login;

import sun.rmi.runtime.Log;

import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// informa o URL para ser acedido
@WebServlet(urlPatterns = "/login.do")
public class LoginServlet extends HttpServlet {

    // variavel referencia para LoginService
    private LoginService userValidService = new LoginService();

    // tratar de metodos GET
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // redirecionar para a pagina pretendida
        request.getRequestDispatcher("/WEB-INF/view/login.jsp").forward(request, response);

    }

    // tratar de metodos Post
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Obter parametros do url browser
        String name = request.getParameter("name");
        String password = request.getParameter("password");

        // validar o login
        boolean isUserValid = userValidService.isUserValid(name, password);

        if (isUserValid) {

            // adicionar o nome na sessao para ser passado pelas paginas redirecionadas
            request.getSession().setAttribute("name", name);

            // redirecionar para a pagina pretendida
            response.sendRedirect("/list-todos.do");


        } else {
            request.setAttribute("errorMessage", "Utilizador invalido!!!");

            // redirecionar para a pagina pretendida
            request.getRequestDispatcher("/WEB-INF/view/login.jsp").forward(request, response);
        }

    }
}
