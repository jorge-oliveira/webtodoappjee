package pt.cjoliveira.filter;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(urlPatterns = "*.do")
public class LoginRequiredFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }


    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws
            IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        System.out.println(request.getRequestURI());

        // obter o atributo name da sessao, e valida se existe user
        if (request.getSession().getAttribute("name") != null) {

            // linha para permitir continuar request depois do filtro, caso contrario para no filtro
            filterChain.doFilter(request, servletResponse);
        } else {
            // caso seja null reencaminha para a pagina de login
            request.getRequestDispatcher("/login.do").forward(servletRequest, servletResponse);
        }


    }

    @Override
    public void destroy() {

    }
}
