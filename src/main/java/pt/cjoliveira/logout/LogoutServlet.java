package pt.cjoliveira.logout;

import pt.cjoliveira.login.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// informa o URL para ser acedido
@WebServlet(urlPatterns = "/logout.do")
public class LogoutServlet extends HttpServlet {

    // tratar de metodos GET
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // invalidar a sessao de quem esta logado
        request.getSession().invalidate();

        // redirecionar para a pagina pretendida
        request.getRequestDispatcher("/WEB-INF/view/login.jsp").forward(request, response);

    }
}
